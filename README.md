Shell scripts to create VLANs and apply traffic control/queue discipline.

In order

vlansetup
removes existing vlans and recreates them.

ipandqueuing
adds queue discipline controls and traffic controls to the outgoing interface.

qstatus
show VLAN queue discipline status

teardown
remove TC and VLANs

dhcpd.conf
dhcp definitions for interfaces.