#!/bin/sh

# Interface
export INF="enp3s0"

# Satellite settings
export SLOSS="1.2%"
export SDELAY="10ms"
export SREORDERLOW="25%"
export SREORDERHIGH="30%"
export SRATE="4mbit"
export SLATENCY="800ms"
export SCORRUPT="10%"
export SBURST="32kbit"

# IP and Queuing for Satellite CA
# IP Setup

# Queuing
tc qdisc del dev ${INF}.sca root
tc qdisc add dev ${INF}.sca root handle 1: netem delay ${SDELAY} reorder ${SREORDERLOW} ${SREORDERHIGH} loss ${SLOSS} corrupt ${SCORRUPT}
tc qdisc add dev ${INF}.sca parent 1: handle 2: tbf rate ${SRATE} burst ${SBURST} latency ${SLATENCY}

