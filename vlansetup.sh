#!/bin/sh

export INF="enp2s0"

# Destroy existing VLANS
ip link set dev ${INF}.suk down
ip link delete ${INF}.suk

ip link set dev ${INF}.sca down
ip link delete ${INF}.sca

ip link set dev ${INF}.sus down
ip link delete ${INF}.sus

ip link set dev ${INF}.snz down
ip link delete ${INF}.snz

ip link set dev ${INF}.sau down
ip link delete ${INF}.sau

ip link set dev ${INF}.ruk down
ip link delete ${INF}.ruk

ip link set dev ${INF}.rca down
ip link delete ${INF}.rca

ip link set dev ${INF}.rus down
ip link delete ${INF}.rus

ip link set dev ${INF}.rnz down
ip link delete ${INF}.rnz

ip link set dev ${INF}.rau down
ip link delete ${INF}.rau

# Create Vlans
# Satellite Leases
ip link add link ${INF} name ${INF}.sau type vlan id 100
ip link add link ${INF} name ${INF}.sca type vlan id 101
ip link add link ${INF} name ${INF}.snz type vlan id 102
ip link add link ${INF} name ${INF}.suk type vlan id 103
ip link add link ${INF} name ${INF}.sus type vlan id 104

# Radios
ip link add link ${INF} name ${INF}.rau type vlan id 200
ip link add link ${INF} name ${INF}.rca type vlan id 201
ip link add link ${INF} name ${INF}.rnz type vlan id 202
ip link add link ${INF} name ${INF}.ruk type vlan id 203
ip link add link ${INF} name ${INF}.rus type vlan id 204



