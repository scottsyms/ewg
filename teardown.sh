#!/bin/sh

# Tear down QOS
tc qdisc del dev eth0.sau root
tc qdisc del dev eth0.sca root
tc qdisc del dev eth0.snz root
tc qdisc del dev eth0.suk root
tc qdisc del dev eth0.sus root
tc qdisc del dev eth0.rau root
tc qdisc del dev eth0.rca root
tc qdisc del dev eth0.rnz root
tc qdisc del dev eth0.ruk root
tc qdisc del dev eth0.rus root

# Bring interfaces down
ip link set dev eth0.sau down
ip link set dev eth0.sca down
ip link set dev eth0.snz down
ip link set dev eth0.suk down
ip link set dev eth0.sus down

ip link set dev eth0.rau down
ip link set dev eth0.rca down
ip link set dev eth0.rnz down
ip link set dev eth0.ruk down
ip link set dev eth0.rus down

# Delete VLANs
ip link delete eth0.sau
ip link delete eth0.sca
ip link delete eth0.snz
ip link delete eth0.suk
ip link delete eth0.sus

ip link delete eth0.rau
ip link delete eth0.rca
ip link delete eth0.rnz
ip link delete eth0.ruk
ip link delete eth0.rus
