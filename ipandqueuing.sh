#!/bin/sh

# Interface
export INF="enp2s0"

# Satellite settings
export SLOSS="1.2%"
export SDELAY="10ms"
export SREORDERLOW="25%"
export SREORDERHIGH="50%"
export SRATE="1mbit"
export SLATENCY="400ms"
export SCORRUPT="10%"
export SBURST="32kbit"

# RF settings
export RLOSS="2.2%"
export RDELAY="10ms"
export RREORDERLOW="25%"
export RREORDERHIGH="50%"
export RRATE="9600bit"
export RLATENCY="400ms"
export RCORRUPT="10%"
export RBURST="32kbit"

# IP and Queuing for Satellite AU
# IP Setup
echo "Setup for Australia"
ip addr add 192.168.100.1/24 brd 192.168.100.255 dev ${INF}.sau
ip link set dev ${INF}.sau up

# Queuing
tc qdisc del dev ${INF}.sau root
tc qdisc add dev ${INF}.sau root handle 1: netem delay ${SDELAY} reorder ${SREORDERLOW} ${SREORDERHIGH} loss ${SLOSS} corrupt ${SCORRUPT}
tc qdisc add dev ${INF}.sau parent 1: handle 2: tbf rate ${SRATE} burst ${SBURST} latency ${SLATENCY}


# IP and Queuing for Satellite CA
# IP Setup
echo "Setup for Australia"
ip addr add 192.168.101.1/24 brd 192.168.101.255 dev ${INF}.sca
ip link set dev ${INF}.sca up

# Queuing
tc qdisc del dev ${INF}.sca root
tc qdisc add dev ${INF}.sca root handle 1: netem delay ${SDELAY} reorder ${SREORDERLOW} ${SREORDERHIGH} loss ${SLOSS} corrupt ${SCORRUPT}
tc qdisc add dev ${INF}.sca parent 1: handle 2: tbf rate ${SRATE} burst ${SBURST} latency ${SLATENCY}


# IP and Queuing for Satellite NZ 
# IP Setup
echo "Setup for New Zealand"
ip addr add 192.168.102.1/24 brd 192.168.102.255 dev ${INF}.snz
ip link set dev ${INF}.snz up

# Queuing
tc qdisc del dev ${INF}.snz root
tc qdisc add dev ${INF}.snz root handle 1: netem delay ${SDELAY} reorder ${SREORDERLOW} ${SREORDERHIGH} loss ${SLOSS} corrupt ${SCORRUPT}
tc qdisc add dev ${INF}.snz parent 1: handle 2: tbf rate ${SRATE} burst ${SBURST} latency ${SLATENCY}


# IP and Queuing for Satellite UK
# IP Setup
echo "Setup for United Kingdom"
ip addr add 192.168.103.1/24 brd 192.168.103.255 dev ${INF}.suk
ip link set dev ${INF}.suk up

# Queuing
tc qdisc del dev ${INF}.suk root
tc qdisc add dev ${INF}.suk root handle 1: netem delay ${SDELAY} reorder ${SREORDERLOW} ${SREORDERHIGH} loss ${SLOSS} corrupt ${SCORRUPT}
tc qdisc add dev ${INF}.suk parent 1: handle 2: tbf rate ${SRATE} burst ${SBURST} latency ${SLATENCY}


# IP and Queuing for Satellite US
# IP Setup
echo "Setup for United States"
ip addr add 192.168.104.1/24 brd 192.168.104.255 dev ${INF}.sus
ip link set dev ${INF}.sus up

# Queuing
tc qdisc del dev ${INF}.sus root
tc qdisc add dev ${INF}.sus root handle 1: netem delay ${SDELAY} reorder ${SREORDERLOW} ${SREORDERHIGH} loss ${SLOSS} corrupt ${SCORRUPT}
tc qdisc add dev ${INF}.sus parent 1: handle 2: tbf rate ${SRATE} burst ${SBURST} latency ${SLATENCY}


# IP and Queuing for RF AU
# IP Setup
echo "Setup for Australia"
ip addr add 192.168.200.1/24 brd 192.168.200.255 dev ${INF}.rau
ip link set dev ${INF}.rau up

# Queuing
tc qdisc del dev ${INF}.rau root
tc qdisc add dev ${INF}.rau root handle 1: netem delay ${RDELAY} reorder ${RREORDERLOW} ${RREORDERHIGH} loss ${RLOSS} corrupt ${RCORRUPT}
tc qdisc add dev ${INF}.rau parent 1: handle 2: tbf rate ${RRATE} burst ${RBURST} latency ${RLATENCY}


# IP and Queuing for RF CA
# IP Setup
echo "Setup for Australia"
ip addr add 192.168.201.1/24 brd 192.168.201.255 dev ${INF}.rca
ip link set dev ${INF}.rca up

# Queuing
tc qdisc del dev ${INF}.rca root
tc qdisc add dev ${INF}.rca root handle 1: netem delay ${RDELAY} reorder ${RREORDERLOW} ${RREORDERHIGH} loss ${RLOSS} corrupt ${RCORRUPT}
tc qdisc add dev ${INF}.rca parent 1: handle 2: tbf rate ${RRATE} burst ${RBURST} latency ${RLATENCY}


# IP and Queuing for RF NZ 
# IP Setup
echo "Setup for New Zealand"
ip addr add 192.168.202.1/24 brd 192.168.202.255 dev ${INF}.rnz
ip link set dev ${INF}.rnz up

# Queuing
tc qdisc del dev ${INF}.rnz root
tc qdisc add dev ${INF}.rnz root handle 1: netem delay ${RDELAY} reorder ${RREORDERLOW} ${RREORDERHIGH} loss ${RLOSS} corrupt ${RCORRUPT}
tc qdisc add dev ${INF}.rnz parent 1: handle 2: tbf rate ${RRATE} burst ${RBURST} latency ${RLATENCY}


# IP and Queuing for RF UK
# IP Setup
echo "Setup for United Kingdom"
ip addr add 192.168.203.1/24 brd 192.168.203.255 dev ${INF}.ruk
ip link set dev ${INF}.ruk up

# Queuing
tc qdisc del dev ${INF}.ruk root
tc qdisc add dev ${INF}.ruk root handle 1: netem delay ${RDELAY} reorder ${RREORDERLOW} ${RREORDERHIGH} loss ${RLOSS} corrupt ${RCORRUPT}
tc qdisc add dev ${INF}.ruk parent 1: handle 2: tbf rate ${RRATE} burst ${RBURST} latency ${RLATENCY}


# IP and Queuing for RF US
# IP Setup
echo "Setup for United States"
ip addr add 192.168.204.1/24 brd 192.168.204.255 dev ${INF}.rus
ip link set dev ${INF}.rus up

# Queuing
tc qdisc del dev ${INF}.rus root
tc qdisc add dev ${INF}.rus root handle 1: netem delay ${RDELAY} reorder ${RREORDERLOW} ${RREORDERHIGH} loss ${RLOSS} corrupt ${RCORRUPT}
tc qdisc add dev ${INF}.rus parent 1: handle 2: tbf rate ${RRATE} burst ${RBURST} latency ${RLATENCY}


# Enable forwarding

cat << EOF >> /etc/sysctl.d/forwarding.conf
net.ipv4.ip_forward = 1
net.ipv4.conf.all.rp_filter=0
EOF

sysctl -p
