#!/bin/sh

tc qdisc show dev eth0.sau
tc qdisc show dev eth0.sca
tc qdisc show dev eth0.snz
tc qdisc show dev eth0.suk
tc qdisc show dev eth0.sus

tc qdisc show dev eth0.rau
tc qdisc show dev eth0.rca
tc qdisc show dev eth0.rnz
tc qdisc show dev eth0.ruk
tc qdisc show dev eth0.rus
